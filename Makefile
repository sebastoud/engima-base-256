NAME_ENCODE = enigma_encode

NAME_DECODE = enigma_decode

SRC_ENCODE =  src/encode/*.c

SRC_DECODE =  src/decode/*.c

FLAGS = -Wall -Wextra -Werror

HELP = READme.md


help :
	cat $(HELP)

set_encode : clean_encode clear compil_encode mv_encode

set_decode : clean_decode clear compil_decode mv_decode

compil_encode :
	gcc $(FLAGS) $(SRC_ENCODE) -o $(NAME_ENCODE)

compil_decode :
	gcc $(FLAGS) $(SRC_DECODE) -o $(NAME_DECODE)

mv_encode :
	mv $(NAME_ENCODE) bin/

mv_decode :
	mv $(NAME_DECODE) bin/

clean_encode :
	rm bin/$(NAME_ENCODE)

clean_decode :
	rm bin/$(NAME_DECODE)

clear :
	clear