#ifndef __GENERATE_SETTINGS_H__
#define __GENERATE_SETTINGS_H__


#define UNSIGNED_CHAR_MAX 256
#define NUMBER_ROTOR 4


#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>


void read_key(			char *path_key,
						unsigned char (*plugboard)[2],
						int *number_elements, 
						unsigned char (*rotors)[256],
						int *rotors_position,
						unsigned char *refractor);


#endif