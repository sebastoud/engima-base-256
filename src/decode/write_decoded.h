#ifndef __WRITE_DECODE_H__
#define __WRITE_DECODE_H__


#define UNSIGNED_CHAR_MAX 256
#define NUMBER_ROTOR 4

#define PATH_DECODED "bin/bin_dest.out"


#include "decode.h"
#include <sys/stat.h>


void decode_binarie(	char *path_encode,
						unsigned char (*plugboard)[2],
						int *number_elements, 
						unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
						int *rotors_position,
						unsigned char *refractor);



#endif