#include "main.h"


void display_perf(char *path_encoded, char *path_key){
	double begin = clock();


	decode_hex_file(path_encoded, path_key);


	double end = clock();

	double time_spent_encode = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Time for decode : %f\n", time_spent_encode);
}



int main(int argc, char const *argv[])
{

	/*
	
		The only parameter the programm takes while launching is the path in bin file
		of the file to encode.

		The function encode_hex_file() will encode this file and return the same file encoded,
		and a file with the key to decode the file, in binarie as well.

		To decode a message, decode_hex_file() will decode the message, with the key in key folder,
		returning the same file decoded. 

	*/

	if (argc == 3)
	{
		decode_hex_file((char*)argv[1], (char*)argv[2]);
	}
	else if (argc == 4)
	{
		display_perf((char*)argv[1], (char*)argv[2]);
	}



	return 0;
}



