#include "decode.h"


void set_rotor_decode_init(unsigned char *sequence, int position)
{

	/*
		Take a sequence and rebase it with position

		Sequence = [1, 3, 9, 6]

		Rebase with  postion of 2
							
		Sequence = [9, 6, 1, 3]

		Rebase 0 do anything 

		Imagine the rotor roll like a clock, this way -> 

		Args :
			sequence : unsigned char *, containing the sequence to rebase
			position : int, the postion to rebase the esequence
	*/
	 
		unsigned char temp_sequence[UNSIGNED_CHAR_MAX];
		for (int i = 0; i < UNSIGNED_CHAR_MAX; i++)
		{
			temp_sequence[i] = sequence[position];
			position++;
			if (position >= UNSIGNED_CHAR_MAX)
			{
				position = 0;
			}
		}
		memcpy(sequence, temp_sequence, UNSIGNED_CHAR_MAX);
} 


void rotors_initialization_decode(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], int *rotors_position)
{

	/*
		Taking the rotors container and for each of the rotors set the sequence to the position 
		associated and then init the current position of the current so 0;

		Args :
			rotors : unsigned char*[256], the rotors container
			rotors_postition : int *, each index is associated to a rotor
	*/

	for (int i = 0; i < NUMBER_ROTOR; i++)
	{
		set_rotor_decode_init(rotors[i], rotors_position[i]);
		rotors_position[i] = 0;
	}
}


void setup_enigma_decode(	char *path_key,
							unsigned char (*plugboard)[2],
							int *number_elements, 
							unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
							int *rotors_position,
							unsigned char *refractor)

{
	/*
		Read the key for decode the message

		This function will init plugboard, rotors sequences and postion, and refractor

		Args:
			plugboard : unsigned char (UNSIGNED_CHAR_MAX/2)][2], each element will contains 2 associated 
			unsigned char, as this form [[104, 213],[20, 41],..,[31, 12]]

			number_elemnets : int, length of plugboard

			rotors : unsigned char**, each line correspond to a rotor with a sequence of unique 
			unsigned char to 0 from 255

			rotors_postions : int*, each element correqpond to the position of the rotors associated 
			by the index

			refractor :unsigned char **, sequence of unique 
			unsigned char to 0 from 255

	*/

	read_key(path_key, plugboard, number_elements ,rotors, rotors_position, refractor);

	rotors_initialization_decode(rotors, rotors_position);


	return;
}


void move_rotors_decode_init(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], int *rotors_position)
{

	/*
		Moving the rotors and actualizing their position
		Every revolution the n+1 rotor will move by 1

		Basically, every time the first rotor will move
		moving others if needed

		Args:
			rotors: unsigned char*[256], the rotors containers
			rotors_postion: the list with all current position of the rotors associated by index
	*/


	int i = 0;
	do 
	{
		set_rotor_decode_init(rotors[i], 1);
		rotors_position[i] += 1;
		if (rotors_position[i] == UNSIGNED_CHAR_MAX)
		{
			rotors_position[i] = 0;
			i++;
			continue;
		}
		else
			return;
	} while(1);

}



void set_rotor_decode_update(unsigned char *sequence, int position)
{

	/*
		Take a sequence and rebase it with position

		Sequence = [1, 3, 9, 6]

		Rebase with  postion of 2
							
		Sequence = [9, 6, 1, 3]

		Rebase 0 do anything 

		Imagine the rotor roll like a clock, this way <-

		Args :
			sequence : unsigned char *, containing the sequence to rebase
			position : int, the postion to rebase the esequence
	*/
	 

		position *= (-1);
		unsigned char temp_sequence[UNSIGNED_CHAR_MAX];
		for (int i = 0; i < UNSIGNED_CHAR_MAX; i++)
		{
			temp_sequence[position] = sequence[i];
			position++;
			if (position >= UNSIGNED_CHAR_MAX)
			{
				position = 0;
			}
		}
		memcpy(sequence, temp_sequence, UNSIGNED_CHAR_MAX);
} 



void move_rotors_decode_update(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], int *rotors_position)
{

	/*
		Moving the rotors and actualizing their position
		Every revolution the n+1 rotor will move by 1

		Basically, every time the first rotor will move
		moving others if needed

		Args:
			rotors: unsigned char*[256], the rotors containers
			rotors_postion: the list with all current position of the rotors associated by index
	*/


	int i = 0;
	do 
	{
		set_rotor_decode_update(rotors[i], -1);
		rotors_position[i] -= 1;
		if (rotors_position[i] == -1)
		{
			rotors_position[i] = UNSIGNED_CHAR_MAX - 1;
			i++;
			continue;
		}
		else
			return;
	} while(1);

}


unsigned char check_plugboard_decode(	unsigned char (*plugboard)[2], 
								int *number_elements,
								unsigned char to_encode)
{

	/*

		As the real plugboard, a unsigned char might associated to another unique one, or not
		Looking at each element of plugboard, where each element has 2 unique unsigned char, that means
		they are associated.

		So, if this unisigned is in plugboard return the associated value, else returning the same value

		Args :
			plugboard : unsigned char*[2], the plugboard
			number_elements : int*, the size of the plugboard
			to_encode : the unisgned char to encode

	*/

	for (int i = 0 ; i < *number_elements; i++)
	{
		if (plugboard[i][0] == to_encode)
		{
			return plugboard[i][1];
		}

		else if (plugboard[i][1] == to_encode)
		{
			return plugboard[i][0];
		}

	}
	return to_encode;
}


unsigned char check_rotors_decode_init(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], unsigned char to_encode)
{

	/*
		The byte to encode pass throw all rotor, where each of them return the index associated to the
		value.

		The value is the byte to encode
		
		Desc order

		Args :
			rotors : unsigned char*[256], the rotors container
			to_encode : the unsigned char to encode

	*/
	for (int i = NUMBER_ROTOR - 1; i >= 0; i--)
	{
		int j = 0;
		while(rotors[i][j] != to_encode)
		{
			j++;
		}
		to_encode = (unsigned char)j;
	}
	return to_encode;
}


unsigned char check_refractor_decode(unsigned char *refractor, unsigned char to_encode)
{

	/*
		Returning the index in refractor by the value

		Value is the unsigned char to encode

		Args:
			refractor : unisgned char*, sequence of unique unsigned char
			to_encode : the unsigned char to encode

	*/

	for (int i = 0; i < UNSIGNED_CHAR_MAX; i++)
	{
		if (refractor[i] == to_encode)
		{
			to_encode = (unsigned char)i;
			break;
		}
	}
	return to_encode;

}


unsigned char check_rotors_decode_out(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], unsigned char to_encode)
{

	/*	

		The byte to encode pass throw all rotor, where each of them return the value associated to the
		index.

		The index is the byte to encode

		Asc order
		
		Args :
			rotors : unsigned char*[256], the rotors container
			to_encode : the unsigned char to encode

	*/

	for (int i = 0; i < NUMBER_ROTOR; i++)
	{
		to_encode = rotors[i][to_encode];
	}
	return to_encode;
}



unsigned char enigma_decode(	unsigned char (*plugboard)[2],
								int *number_elements, 
								unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
								int *rotors_position,
								unsigned char *refractor,
								unsigned char to_encode)
{

	/*
		As enigma, every time a key is pressed (a byte is read in our case)
		But we are decoding, so we moving back from the end
		The rotors is moving rigth after the a key is pressed, then going in those setp:

		byte -> plugboard -> rotors(in asc order) -> refractor -> rotors(in desc order) -> plugboard -> byte_encoded 

		Notice :
			We are decoding so the rotors moves after the decode of the variable
			Roughly the same thing proccessus in reverse
	*/


	

	to_encode = check_plugboard_decode(plugboard, number_elements, to_encode);

	to_encode = check_rotors_decode_out(rotors, to_encode);

	to_encode = check_refractor_decode(refractor, to_encode);

	to_encode = check_rotors_decode_init(rotors, to_encode);

	to_encode = check_plugboard_decode(plugboard, number_elements, to_encode);

	move_rotors_decode_update(rotors, rotors_position);

	return to_encode;

}


void init_file_decode(){
	system("touch bin/bin_dest.out");
}

void delete_file(){
	system("rm encoded_files/encoded.out");
}


void decode_hex_file(char *path_encoded, char *path_key){

	/*
		Create the destination of decoding
	*/


	init_file_decode();
	
	/*
		Declare an integer that will contains the number of elements in the plugboard
	*/

	int number_elements;

	/*
		Declare the plugboard
	*/

	unsigned char plugboard[UNSIGNED_CHAR_MAX/2][2];
	
	/*
		Declare the rotors holder, each element is the sequence of the rotor associated by the index
	*/

	unsigned char rotors[NUMBER_ROTOR][UNSIGNED_CHAR_MAX];

	/*
		The position of each rotor associated by the index
	*/

	int rotors_position[NUMBER_ROTOR];

	/*
		Declare the refractor sequence
	*/

	unsigned char refractor[UNSIGNED_CHAR_MAX];

	/*
		Calling the initialization of enigma decode
	*/


	setup_enigma_decode(path_key, plugboard, &number_elements, rotors, rotors_position, refractor);

	/*
		For each byte of the encoded file:
		
		Every time we are reading a byte from the encoded file, this one get decoded passing throw enigma
		machine and then wrote into the dest file.

	*/

	decode_binarie(path_encoded, plugboard, &number_elements, rotors, rotors_position, refractor);

	/*
		Deleting file
	*/

	delete_file();

	return;
}