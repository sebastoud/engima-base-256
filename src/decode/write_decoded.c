#include "write_decoded.h"


size_t size_file(char *path_encoded){
	/*
		Return the size of the binarie to decode

		Args:
			path_encoded : char*, the path to the file to decode

		Returns:
			size_file : size_t, the current size of the file
	*/


	/*
		Decalare a stat structure
	*/

	struct stat size;

	/*
		st will permit to get the size of the file
	*/

	stat(path_encoded, &size);


	return size.st_size;
}


void write_dest(	unsigned char (*plugboard)[2],
					int *number_elements, 
					unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
					int *rotors_position,
					unsigned char *refractor,
					unsigned char (*text),
					size_t size_bin)
{

	/*
		This function will take each byte of the file read, of decode it from the end, and then write the actual 
		new tab (text) int the destination file

		Args :
			text = unsigned char*, the current binarie to decode
			size_bin =  size_t, the current size of the file 

	*/

	/*
		Init the file descriptor of the decoded file
	*/

	int fd_decoded = open(PATH_DECODED, O_WRONLY);


	/*
		For each element in text, from the end, decode the character
	*/

	for (int index = size_bin - 1; index >= 0; index--) 
	{
		text[index] = enigma_decode(plugboard, number_elements, rotors, rotors_position, refractor, text[index]);
    }

    /*
		Declare the buf to write in the file
	*/

	unsigned char buf[1];


	/*
		Write each element of the file with a buffer
	*/

    for (int index = 0; index < (int)size_bin; index++)
    {
    	buf[0] = text[index];
    	write(fd_decoded, (const void*)(buf), (sizeof(buf)));
    }

    /*
		Close the file descriptor
    */

    close(fd_decoded);
}


void decode_binarie(	char *path_encoded,
						unsigned char (*plugboard)[2],
						int *number_elements, 
						unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
						int *rotors_position,
						unsigned char *refractor)

{

	/*
		This function will read the encoded message,
		store it, and init the rotors to their final position when encoding
	*/

	/*
		Store the size of a binarie
	*/

	size_t size_bin = size_file(path_encoded);

	/*
		The binarie will be stored here
	*/

	unsigned char text[size_bin];


	/*
		Init the buffer to read the file
	*/

	unsigned char buf[1];

	/*
		Open the file descriptor in with the path to the encoded file
	*/

	int fd_encoded = open(path_encoded, O_RDONLY);

	/*
		Moving the rotors to the end position of encoding
	*/

	int i;
	for (i = 0; read(fd_encoded, buf, sizeof(buf)) != 0; i++)
	{	
		text[i] = buf[0];
	}
	i -= 1;
	for (int rot = 0; rot < NUMBER_ROTOR && i != 0; rot++)
	{
		rotors_position[rot] = (i) % (UNSIGNED_CHAR_MAX);
		set_rotor_decode_init(rotors[rot], (i) % (UNSIGNED_CHAR_MAX));
		i /= UNSIGNED_CHAR_MAX;
	}

	move_rotors_decode_init(rotors, rotors_position);

	/*
		Closing the file descriptor
	*/

	close(fd_encoded);
	

	/*
		With all things init, we can can decode each byte of text and write it
		into the dest file
	*/

	write_dest(plugboard, number_elements, rotors, rotors_position, refractor, text, size_bin);


}