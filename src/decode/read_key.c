#include "read_key.h"


void read_key(			char *path_key,
						unsigned char (*plugboard)[2],
						int *number_elements, 
						unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
						int *rotors_position,
						unsigned char *refractor)

{

	/*

		Reading the key file to init the settings on enigma

		Args:
			plugboard : unsigned char (UNSIGNED_CHAR_MAX/2)][2], each element will contains 2 associated 
			unsigned char, as this form [[104, 213],[20, 41],..,[31, 12]]

			number_elemnets : int, length of plugboard

			rotors : unsigned char**, each line correspond to a rotor with a sequence of unique 
			unsigned char to 0 from 255

			rotors_postions : int*, each element correqpond to the position of the rotors associated 
			by the index

			refractor :unsigned char **, sequence of unique 
			unsigned char to 0 from 255

	*/

	/*
		Opening the file descriptor of the file
	*/

	int fd_key = open(path_key, O_RDONLY);

	/*
		First byte that will be write is the number of elements 
	*/

	unsigned char number_temp; 

	read(fd_key, &number_temp, sizeof(number_temp));

	*number_elements = (int)number_temp;


	/*
		Then, read every element in plugboard in asc order by 2 
	*/

	unsigned char buf_plugboard[2];

	for (int i = 0; i < *number_elements; i++){
		read(fd_key, buf_plugboard, sizeof(buf_plugboard));
		memcpy(plugboard[i], buf_plugboard, 2);
	}

	/*
		Then for each rotors, read the sequence of the rotor and the position rigth after
	*/


	for (int i = 0; i < NUMBER_ROTOR; i++){
		unsigned char buf_rotor[UNSIGNED_CHAR_MAX];
		read(fd_key, buf_rotor, sizeof(buf_rotor));
		memcpy(rotors[i], buf_rotor, UNSIGNED_CHAR_MAX);


		unsigned char buf_pos[1];
		read(fd_key, buf_pos, sizeof(buf_pos));
		rotors_position[i] = (int)buf_pos[0];
	}

	/*
		Then read each element of the refractor
	*/

	unsigned char buf_refractor[UNSIGNED_CHAR_MAX];
	read(fd_key, buf_refractor, sizeof(buf_refractor));
	memcpy(refractor, buf_refractor, UNSIGNED_CHAR_MAX);

	/*
		Close the fd and delete the key
	*/

	close(fd_key);

    system("rm key/key.out");

    return;
}
