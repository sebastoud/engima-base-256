#ifndef __DECODE_H__
#define __DECODE_H__

#define UNSIGNED_CHAR_MAX 256
#define NUMBER_ROTOR 4

#include "read_key.h"
#include "write_decoded.h"


unsigned char enigma_decode(	unsigned char (*plugboard)[2],
								int *number_elements, 
								unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
								int *rotors_position,
								unsigned char *refractor,
								unsigned char to_encode);


void move_rotors_decode_init(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], int *rotors_position);

void set_rotor_decode_init(unsigned char *sequence, int position);

void decode_hex_file(char *path_encoded, char *path_key);


#endif