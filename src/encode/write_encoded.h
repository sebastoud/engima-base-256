#ifndef __WRITE_ENCODED_H__
#define __WRITE_ENCODED_H__


#define PATH_DEST "encoded_files/encoded.out"
#define UNSIGNED_CHAR_MAX 256

#include "encode.h"

void encode_binarie(char *path_src,
					unsigned char (*plugboard)[2],
					int *number_elements, 
					unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
					int *rotors_position,
					unsigned char *refractor);


#endif