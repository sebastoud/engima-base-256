#ifndef __ENCODE_H__
#define __ENCODE_H__

#define UNSIGNED_CHAR_MAX 256
#define NUMBER_ROTOR 4


#include "generate_settings_encode.h"
#include "write_key_settings.h"
#include "write_encoded.h"
#include <time.h>


unsigned char enigma_encode(	unsigned char (*plugboard)[2],
								int *number_elements, 
								unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
								int *rotors_position,
								unsigned char *refractor,
								unsigned char to_encode);


void encode_hex_file(char *path_src);



#endif