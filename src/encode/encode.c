#include "encode.h"


void random_generation(	unsigned char (*plugboard)[2],
						int *number_elements, 
						unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
						int *rotors_position,
						unsigned char *refractor)
{

	/*
		This function will init sequence of plugboard, rotors and postion, and refractor

		Args :
			plugboard : unsigned char (UNSIGNED_CHAR_MAX/2)][2], each element will contains 2 associated 
			unsigned char, as this form [[104, 213],[20, 41],..,[31, 12]]

			number_elemnets : int, length of plugboard

			rotors : unsigned char**, each line correspond to a rotor with a sequence of unique 
			unsigned char to 0 from 255

			rotors_postions : int*, each element correqpond to the position of the rotors associated 
			by the index

			refractor :unsigned char **, sequence of unique 
			unsigned char to 0 from 255
	*/

	random_unique_sequence_plugboard(plugboard, number_elements);
	
	for (int i = 0; i < NUMBER_ROTOR; i++)
	{
		random_unique_sequence_unsigned(rotors[i]);
		rotors_position[i] = rand() % UNSIGNED_CHAR_MAX;
	}

	random_unique_sequence_unsigned(refractor);

	return;
}


void set_rotor(unsigned char *sequence, int position)
{

	/*
		Take a sequence and rebase it with position

		Sequence = [1, 3, 9, 6]

		Rebase with  postion of 2
							
		Sequence = [9, 6, 1, 3]

		Rebase 0 do anything 

		Imagine the rotor roll like a clock, this way -> 

		Args :
			sequence : unsigned char *, containing the sequence to rebase
			position : int, the postion to rebase the esequence

	*/ 


	unsigned char temp_sequence[UNSIGNED_CHAR_MAX];
	for (int i = 0; i < UNSIGNED_CHAR_MAX; i++)
	{
		temp_sequence[i] = sequence[position];
		position++;
		if (position >= UNSIGNED_CHAR_MAX)
		{
			position = 0;
		}
	}
	memcpy(sequence, temp_sequence, UNSIGNED_CHAR_MAX);
}


void rotors_initialization(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], int *rotors_position)
{

	/*
		Taking the rotors container and for each of the rotors set the sequence to the position 
		associated and then init the current position of the current so 0;

		Args :
			rotors : unsigned char*[256], the rotors container
			rotors_postition : int *, each index is associated to a rotor
	*/

	for (int i = 0; i < NUMBER_ROTOR; i++)
	{
		set_rotor(rotors[i], rotors_position[i]);
		rotors_position[i] = 0;
	}
}


void setup_enigma(	unsigned char (*plugboard)[2],
					int *number_elements, 
					unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
					int *rotors_position,
					unsigned char *refractor)
{

	/*
		This function will init plugboard, rotors sequences and postion, and refractor

		Writing the key to decode the message

		And then initialize the rotors with the positions previously found

		Args:
			plugboard : unsigned char (UNSIGNED_CHAR_MAX/2)][2], each element will contains 2 associated 
			unsigned char, as this form [[104, 213],[20, 41],..,[31, 12]]

			number_elemnets : int, length of plugboard

			rotors : unsigned char**, each line correspond to a rotor with a sequence of unique 
			unsigned char to 0 from 255

			rotors_postions : int*, each element correqpond to the position of the rotors associated 
			by the index

			refractor :unsigned char **, sequence of unique 
			unsigned char to 0 from 255

	*/

	random_generation(plugboard, number_elements, rotors, rotors_position, refractor);

	setup_key(plugboard, number_elements, rotors, rotors_position, refractor);

	rotors_initialization(rotors, rotors_position);

}


void move_rotors(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], int *rotors_position)
{

	/*
		Moving the rotors and actualizing their position
		Every revolution the n+1 rotor will move by 1

		Basically, every time the first rotor will move
		moving others if needed

		Args:
			rotors: unsigned char*[256], the rotors containers
			rotors_postion: the list with all current position of the rotors associated by index
	*/


	int i = 0;
	do 
	{
		set_rotor(rotors[i], 1);
		rotors_position[i] += 1;
		if (rotors_position[i] == UNSIGNED_CHAR_MAX)
		{
			rotors_position[i] = 0;
			i++;
			continue;
		}
		else
			return;
	} while(1);

}


unsigned char check_plugboard(	unsigned char (*plugboard)[2], 
								int *number_elements,
								unsigned char to_encode)
{

	/*

		As the real plugboard, a unsigned char might associated to another unique one, or not
		Looking at each element of plugboard, where each element has 2 unique unsigned char, that means
		they are associated.

		So, if this unisigned is in plugboard return the associated value, else returning the same value

		Args :
			plugboard : unsigned char*[2], the plugboard
			number_elements : int*, the size of the plugboard
			to_encode : the unisgned char to encode

	*/

	for (int i = 0 ; i < *number_elements; i++)
	{
		if (plugboard[i][0] == to_encode)
		{
			return plugboard[i][1];
		}

		else if (plugboard[i][1] == to_encode)
		{
			return plugboard[i][0];
		}

	}
	return to_encode;
}

unsigned char check_rotors_in(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], unsigned char to_encode)
{

	/*
		The byte to encode pass throw all rotor, where each of them return the value associated to the
		index.

		The index is the byte to encode
		
		Asc order

		Args :
			rotors : unsigned char*[256], the rotors container
			to_encode : the unsigned char to encode

	*/

	for (int i = 0; i < NUMBER_ROTOR; i++)
	{
		to_encode = rotors[i][to_encode];
	}
	return to_encode;
}


unsigned char check_refractor(unsigned char *refractor, unsigned char to_encode)
{

	/*
		Returning the value in refractor by the index

		Index is the unsigned char to encode

		Args:
			refractor : unisgned char*, sequence of unique unsigned char
			to_encode : the unsigned char to encode

	*/

	return refractor[to_encode];
}


unsigned char check_rotors_out(unsigned char (*rotors)[UNSIGNED_CHAR_MAX], unsigned char to_encode)
{

	/*	

		The byte to encode pass throw all rotor, where each of them return the index associated to the
		value.

		The value is the byte to encode

		Desc order
		
		Args :
			rotors : unsigned char*[256], the rotors container
			to_encode : the unsigned char to encode

	*/

	for (int i = NUMBER_ROTOR - 1; i >= 0; i--)
	{
		int j = 0;
		while(rotors[i][j] != to_encode)
		{
			j++;
		}
		to_encode = (unsigned char)j;
	}
	return to_encode;
}


unsigned char enigma_encode(	unsigned char (*plugboard)[2],
								int *number_elements, 
								unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
								int *rotors_position,
								unsigned char *refractor,
								unsigned char to_encode)
{

	/*
		As enigma, every time a key is pressed (a byte is read in our case)
		The rotors is moving rigth after the a key is pressed, then going in those setp:

		byte -> plugboard -> rotors(in asc order) -> refractor -> rotors(in desc order) -> plugboard -> byte_encoded 
	*/


	move_rotors(rotors, rotors_position);



	to_encode = check_plugboard(plugboard, number_elements, to_encode);

	to_encode = check_rotors_in(rotors, to_encode);

	to_encode = check_refractor(refractor, to_encode);

	to_encode = check_rotors_out(rotors, to_encode);

	to_encode = check_plugboard(plugboard, number_elements, to_encode);

	return to_encode;

}


void init_file_encode()
{

	/*
		Creating files with system() function
		Two binaries in distinct folder, the key to decode, and the binaraie that will be
		the source file encoded 
	*/


	system("touch key/key.out");
	system("touch encoded_files/encoded.out");

	return;
}


void encode_hex_file(char *path_src)
{

	/*
		Init they key file and the binaraie that will be encoded
	*/

	init_file_encode();

	/*
		Has to init srand() to "truly" set the random sequence
	*/

	srand(time(NULL));


	/*
		Declare an integer that will contains the number of elements in the plugboard
	*/

	int number_elements;

	/*
		Then init this number with a random betwenn 2 and 127
	*/

	random_number_element(&number_elements);

	/*
		Declare the plugboard
	*/

	unsigned char plugboard[number_elements][2];

	/*
		Declare the rotors holder, each element is the sequence of the rotor associated by the index
	*/

	unsigned char rotors[NUMBER_ROTOR][UNSIGNED_CHAR_MAX];

	/*
		The position of each rotor associated by the index
	*/

	int rotors_position[NUMBER_ROTOR];

	/*
		Declare the refractor sequence
	*/

	unsigned char refractor[UNSIGNED_CHAR_MAX];


	/*
		Calling the initialization of enigma encode
	*/

	setup_enigma(plugboard, &number_elements, rotors, rotors_position, refractor);

	/*
		For each byte of the source file:

		Every time we are reading a byte from the source file, this one get encoded passing throw enigma
		machine and then wrote into the encoded file.


	*/

	encode_binarie(path_src, plugboard, &number_elements, rotors, rotors_position,refractor);


	return;
}

