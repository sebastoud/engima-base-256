#ifndef __WRITE_KEY_SETTINGS_H__
#define __WRITE_KEY_SETTINGS_H__

#define PATH_KEY "key/key.out"

#define UNSIGNED_CHAR_MAX 256
#define NUMBER_ROTOR 4

#include <unistd.h>
#include <fcntl.h>
#include <string.h>


void setup_key(		unsigned char (*plugboard)[2],
					int *number_elements, 
					unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
					int *rotors_position,
					unsigned char *refractor);


#endif