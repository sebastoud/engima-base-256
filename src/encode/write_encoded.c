#include "write_encoded.h"


void encode_binarie(char *path_src,
					unsigned char (*plugboard)[2],
					int *number_elements, 
					unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
					int *rotors_position,
					unsigned char *refractor)
{

	/*

	Declaring file descriptor of :	read -> the source file to encode
									write -> the dest file, that will contains the source file encoded

	*/

	int fd_read, fd_write;

	fd_read 	= open(path_src, O_RDONLY);
	fd_write 	= open(PATH_DEST, O_WRONLY);



	unsigned char buf[1];
	while (read(fd_read, buf, sizeof(buf)) != 0) 
	{	
		buf[0] = enigma_encode(plugboard, number_elements, rotors, rotors_position,refractor, buf[0]);
    	write(fd_write, (const void*)(buf), (size_t)(sizeof(buf)));
    }

    //Closing file_descriptor

	close(fd_read);
	close(fd_write);


}
