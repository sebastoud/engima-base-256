#include "write_key_settings.h"


void setup_key(		unsigned char (*plugboard)[2],
					int *number_elements, 
					unsigned char (*rotors)[UNSIGNED_CHAR_MAX],
					int *rotors_position,
					unsigned char *refractor)

{

	/*

		Writing the current settings in as key file to decode the message

		Args:
			plugboard : unsigned char (UNSIGNED_CHAR_MAX/2)][2], each element will contains 2 associated 
			unsigned char, as this form [[104, 213],[20, 41],..,[31, 12]]

			number_elemnets : int, length of plugboard

			rotors : unsigned char**, each line correspond to a rotor with a sequence of unique 
			unsigned char to 0 from 255

			rotors_postions : int*, each element correqpond to the position of the rotors associated 
			by the index

			refractor :unsigned char **, sequence of unique 
			unsigned char to 0 from 255

	*/


	/*
		Opening the file descriptor of the file
	*/

	int fd_key = open(PATH_KEY, O_WRONLY);

	/*
		First byte that will be write is the number of elements 
	*/

	unsigned char buf_plugboard[2];
	buf_plugboard[0] = (unsigned char)(*number_elements);

	write(fd_key, &buf_plugboard, 1);

	/*
		Then, write every element in plugboard in asc order by 2 
	*/

	for (int i = 0; i < *number_elements; i++)
	{
		memcpy(buf_plugboard, (const void*)plugboard[i], 2);
		write(fd_key, buf_plugboard, sizeof(buf_plugboard));
	}

	/*
		Then for each rotors, write the sequence of the rotor and the position rigth after
	*/


	for (int i = 0; i < NUMBER_ROTOR; i++)
	{
		unsigned char buf_rotor[UNSIGNED_CHAR_MAX];
		memcpy(buf_rotor, (const void*)rotors[i], UNSIGNED_CHAR_MAX);
		write(fd_key, buf_rotor, sizeof(buf_rotor));


		unsigned char buf_pos[1];
		memcpy(buf_pos, (const void*)&rotors_position[i], 1);
		write(fd_key, buf_pos, 1);

	}

	/*
		Then write each element of the refractor
	*/

	unsigned char buf_refractor[UNSIGNED_CHAR_MAX];
	memcpy(buf_refractor, (const void*)refractor, UNSIGNED_CHAR_MAX);
	write(fd_key, buf_refractor, sizeof(buf_refractor));

	close(fd_key);

}
