#ifndef __GENERATE_SETTINGS__
#define __GENERATE_SETTINGS__

#define UNSIGNED_CHAR_MAX 256


#include <unistd.h>
#include <stdlib.h>
#include <string.h>


void random_unique_sequence_plugboard(unsigned char (*sequence)[2], int *number_elements);

void random_unique_sequence_unsigned(unsigned char *sequence);

void random_number_element(int *number_elements);

#endif