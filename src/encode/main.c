#include "main.h"


void display_perf(char *path_src){
	double begin = clock();


	encode_hex_file(path_src);


	double end = clock();

	double time_spent_encode = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Time for encode : %f\n", time_spent_encode);
}



int main(int argc, char const *argv[])
{

	/*
	
		The only parameter the programm takes while launching is the path in bin file
		of the file to encode.

		The function encode_hex_file() will encode this file and return the same file encoded,
		and a file with the key to decode the file, in binarie as well.

		To decode a message, decode_hex_file() will decode the message, with the key in key folder,
		returning the same file decoded. 

	*/

	if (argc == 2)
	{
		encode_hex_file((char*)argv[1]);
	}
	else if (argc == 3)
	{
		display_perf((char*)argv[1]);
	}



	return 0;
}



