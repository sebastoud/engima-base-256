#include "generate_settings_encode.h"


void random_unique_sequence_plugboard(unsigned char (*sequence)[2], int *number_elements)
{

	/*
		Initialization of the plugboard
	
		First, temp_sequence will be full of unique character from 0 to 255
		as this form [[0, 1],[2, 3],..,[126, 127]]

		Ascendant order

		Then, swap 2 random index in temp_sequence 512 times
		(with one instruction(i % 0) to make to it more random the number is mayber overkill)

		Thus, copy n element where (n = number_element) of temp_sequence in asc order to the real
		plugboard 

		Args :
			sequence : unsigned char (*)[2], the current allocated memory of the plugboard to randomize in 2D where each line
			has 2 elements

			number_elements : int, the variable length of the plugboard

	*/

	unsigned char temp_sequence[UNSIGNED_CHAR_MAX/2][2];

	for (int i = 0; i < UNSIGNED_CHAR_MAX; i+= 2)
	{
		temp_sequence[i/2][0] = (unsigned char)i;
		temp_sequence[i/2][1] = (unsigned char)i + 1;
	}

	int random_a;
	int random_b;

	unsigned char temp_a_a;
	unsigned char temp_a_b;

	unsigned char temp_b_a;
	unsigned char temp_b_b;

	for (int i = 0; i < UNSIGNED_CHAR_MAX; i++)
	{


		//Random index

		random_a = (rand() % UNSIGNED_CHAR_MAX) / 2;
		random_b = (rand() % UNSIGNED_CHAR_MAX) / 2;


		//Init temp for swaping

		temp_a_a = temp_sequence[random_a][0];
		temp_a_b = temp_sequence[random_a][1];

		temp_b_a = temp_sequence[random_b][0];
		temp_b_b = temp_sequence[random_b][1];

		
		//Swap (that i % 2 is necessary without it there is no random)

		if (i % 2)
		{
			temp_sequence[random_b][0] = temp_a_b;
			temp_sequence[random_a][1] = temp_b_a;
		}
		else 
		{
			temp_sequence[random_a][0] = temp_b_b;
			temp_sequence[random_b][1] = temp_a_a;
		}
	}

	//Copy n element of temp_sequence into plugboard

	for (int i = 0; i < *number_elements; i++)
	{	
		memcpy(sequence[i], temp_sequence[i], 2);
	}

	return;
}


void random_unique_sequence_unsigned(unsigned char *sequence)
{

	/*
		Initialization of unique 256 unsigned char in 1D (refractor, rotors)


		First, a table of unsigned char, with unique unsigned from 0 to 255
		asc order

		Then swap them from random index 512 times (maybe overkill)


		Args :
			sequence : unsigned char*, the current allocated memory to randomize
	*/

	for (int i = 0; i < UNSIGNED_CHAR_MAX; i++)
	{
		sequence[i] = (unsigned char)i;
	}

	for (int i = 0; i < (UNSIGNED_CHAR_MAX * 2); i++)
	{
		int random_a = rand() % UNSIGNED_CHAR_MAX;
		int random_b = rand() % UNSIGNED_CHAR_MAX;
		unsigned char temp = sequence[random_a];
		sequence[random_a] = sequence[random_b];
		sequence[random_b] = temp;
	}
	return;
}


void random_number_element(int *number_elements)
{

	/*
		This function return an integer from 2, to 127
		and loop on it if the number is not in born

		Args;
			number_elements : int*,  value directly modified
	*/

	do 
	{
		*number_elements = ((rand() % (UNSIGNED_CHAR_MAX/2)) + 1);
	} while(*number_elements < 1 || *number_elements > (UNSIGNED_CHAR_MAX/2) - 1);
	
	return;
}

