# Enigma base 256

Enigma encryption

## Some story(not that much)

During WW2, the NAZI found a way to communicate.
Crypting their messages.
The name of this machine is enigma/
end.


### How enigma is working?

The real enigma proccessing was pure mechanism, so there is a current between the input and outpout.

Imagine a keyboard with only alphabet letters, every time a key is pressed, a ligth is turning on telling wich charachter the input will correspond. (A -> Z) example..

With the same setttings as encoded, you will be albe to decode, nothing new.

Enigma machine a 3 main step, the plugboard, the rotors and the refractor.


Plugboard :

	Rigth after pressing a key, the first step, is the plugboard.
	Under neath the keyboard, there is litterally a plud device that allow to the user to combine
	unique letter between each other, 10 exacly, so for example:
		If the plusboard is linking A with Z, when you will press the key A, Z will come out, and works in both ways, Z to A.
		A letter can be alone, and return the same letter while passing throught plugboard.
		Very impotant, all letters linked has to be unique and linked once.
		[[A, C], [I, K], [T, Y], ..., [J, R]]  


Rotors :
	
		The letter, after the plugboard, will pass throught the first rotors, the second, etc..
		Rotors ?
			Visualize, 2 part in this, one with a static sequence of alphabet in asc order, and
			the second part a sequence of alphabet in disorder.
			Basically, it a sequence of the 26 letters of alphabet in certain order associated to the alphabet it self, for example : 
		In:		[A,B,C,D,E,..,Y,Z]
		Out:	[A,R,V,S,C,..,R,B]

		In this case if C in come in, the rotor will return V.
		If it was A its A.

		Now visualize 3 of them where each input and output is linked.

Refractor :

		Enigma machine is not magic, after passing trow all those step, the current, needs to go back
		to turn on the ligth.
		It's a static rotor if you like, with every letter associated to is different one(it's really important) .
		Each letter coming in the refractor will return a different letter, to make the current different		

(Example)Admit that letter pressed is A, the plugboard will return C, then the first rotor will return the value associated to C, this for each rotors in asc order.
After that, C became K after all those step (Just example), goes in the refractor that will return a B 
(Must be different), from here, the current must came back to turn on the light, B will pass trought the 
Last rotor, the second, then the first, in desc order, and then came once aigain in the plugboard and turn on the corresping light

It's not the end, each time a key is pressed, rotors moving..

So for WW2, nice encode device.

Every time a key is pressed the first rotor will turn by 1 and after a revolution, the seconde one will move, etc..


### The settings of enigma

To be alble to encode and decode a message, there are some settings (key) that will set the enigma machine is certains way that she we be able to encode the message in certain form


The plugboard plugs is the first setting telling wich letter is linked with

The rotors, (5 rotors was aviable and they choose tree of those), wich rotors cill be used, and in wich position.
As you might notice, the rotors ar moving, so as expected we can define default positon of rotor to increase the number of possibilities.
Each chosen rotors as a postion value coming from 1 to 26.

The refractor, it's a settings, (enigma weakness) some sequence of refractor was definied but
from what, I know, there was like , REFRACTOR A, B, C , B THIN, things like that, but a sequence of
refractor needs to be define


### Enigma base 256

Now you see what this programm is here..

Previous enigma was able to hold 26 letters in 3 rotors (summerize)

Inspired by this machine, this programm will encode any binarie file comming from 0 to 255

The machine is easy to reproduce as a programm.

This is not the exact step by step enigma for performance reason.

The enigma aviable here has only 3 rotors, but values in the header has to change to work with as much as MAX_INT.


Basically the formula to see how much possiblities there is.

N is the number of rotors

```
(factorial(256)^N)*(256^N)*(126*factorial(256)) * factorial(256)
```

For only 3 rotos:

```
9.8876e+2543
```


### Install

Download enigma files (.zip, git clone, etc).

```
for those command works, make sure that this folder is named engima
```

And that's it


### Using Encode


Put the file where you want.

All has be comiled so no pre-command has be be used

```
cd enigma
```

For example I will use bin_src.out already present in the file.

engima_encode is an .out that will take the path a the file from engima, bin_src.out here

```
./bin/enigma_encode bin/bin_src.out
```

After executing the command, the current binarie has been encoded, in encoded_files, and the the in the key folder
as key.out

Those file will be necassary to decode the message.

```
mv encoded_filed/encoded.out #Where you want
```

```
mv key/key.out #Where you want
```

Make sure thos folder are empty for future encode.

### Using Decode


With the encoded file and the key.
You will be able to decode.


```
cd enigma
```

For that move your key.out in key folder
and the encoded .out in encoded_filed folder


For example I will use encoded.out default name of the encoded binarie.

And key.out in key foler, default name as well

engima_encode is an .out that will take the path a the file from engima, bin_src.out here

```
./bin/enigma_decode encoded_files/encoded.out key/key.out
```

After executing the command, the encoded bin has been decoded, in bin folder as bin_dest.out

They key and encoded file will be removed executing the command

Move the dest file for future decode.

```
mv bin/bin_dest.out #Where you want
```


## Open source

There is a Makefile to compil encode and decode version
with -Wall -Wextra -Werror

```
make compil_encode
make compil_decode
```

Combining some rules, this will clear the console, clean the previous .out ,compil, move the the bin folder 

```
make set_encode
make set_decode
```

Calling make alone with show this message

### Modify the number of rotors

Go in the header file and change the number of rotor by 3 to MAX_INT
Easy to find, but make that you modifing each file

```
Google documentation

You can run the performance by adding another paramters when executin the programm to see the speed

./bin/enigma_encode bin/bin_src.out 2
./bin/enigma_encode bin/bin_src.out "hi"
./bin/enigma_encode bin/bin_src.out hi

Just one more paramter, same for decode

./bin/enigma_decode encoded_files/encoded.out key/key.out "j"
./bin/enigma_decode encoded_files/encoded.out key/key.out 43
./bin/enigma_decode encoded_files/encoded.out key/key.out oezfjpz

Why? I'm lazy
```

## Built In

* C

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.
 

## Authors

* Raphael Cohen


## Inspiration

* This man will explain much better https://www.youtube.com/watch?v=2D2bJWHvqJo&t=534s

